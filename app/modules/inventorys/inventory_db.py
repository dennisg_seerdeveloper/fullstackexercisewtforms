from bson.objectid import ObjectId

class InventoryDB:
	def __init__(self, conn):
		self.conn = conn

	def getInventoryAll(self):
		return self.conn.find()

	def getInventorybyObjid(self, objectid):
		return self.conn.find({'_id': ObjectId(objectid)})

	def getInventorybyUsername(self, username):
		return self.conn.find({'username': username})

	def createInventory(self, inventory):
		inventory_id = self.conn.insert({'itemname':inventory.itemname, 
											'price': inventory.price, 
											'quantity': inventory.quantity, 
											'username': inventory.username})
		return inventory_id

	def updateInventory(self, inventory):
		self.conn.update_one({'_id': ObjectId(inventory.ids)}, { "$set": {'itemname':inventory.itemname, 
											'price': inventory.price, 
											'quantity': inventory.quantity, 
											'username': inventory.username}})

	def updateInventoryQuantity(self, inventory):
		self.conn.update_one({'_id': ObjectId(inventory.ids)}, { "$inc": { 
											'quantity': inventory.quantity}})

	def deleteInventory(self, ids):
		self.conn.delete_one({'_id': ObjectId(ids)})