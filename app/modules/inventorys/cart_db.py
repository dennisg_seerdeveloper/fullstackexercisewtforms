from bson.objectid import ObjectId

class CartDB:
	def __init__(self, conn):
		self.conn = conn

	def getCartbyUsername(self, username):
		return self.conn.find({'username': username})

	def createCart(self, itemname, price, totalprice, quantity, username):
		cart_id = self.conn.insert({'itemname':itemname, 
											'price': price, 
											'totalprice': totalprice, 
											'quantity': quantity, 
											'username': username})
		return cart_id

	def updateCart(self, inventory):
		self.conn.update_one({'_id': ObjectId(inventory.ids)}, { "$set": {'itemname':inventory.itemname, 
											'price': inventory.price, 
											'totalprice': inventory.totalprice, 
											'quantity': inventory.quantity, 
											'username': inventory.username}})

	def updateCartQuantity(self, inventory):
		self.conn.update_one({'_id': ObjectId(inventory.ids)}, { "$inc": { 
											'quantity': inventory.quantity}})

	def deleteCart(self, ids):
		self.conn.delete_one({'_id': ObjectId(ids)})