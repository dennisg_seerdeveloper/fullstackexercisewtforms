from bson.objectid import ObjectId

class OrderDB:
	def __init__(self, conn):
		self.conn = conn

	def getOrderAll(self):
		return self.conn.find()

	def getOrderbyObjid(self, objectid):
		return self.conn.find({'_id': ObjectId(objectid)})

	def getOrderbyUsername(self, username):
		return self.conn.find({'username': username})

	def createOrder(self, order):
		Order_id = self.conn.insert({'itemname':order.itemname, 
											'price': order.price, 
											'quantity': order.quantity, 
											'username': order.username})
		return Order_id

	def updateOrder(self, order):
		self.conn.update_one({'_id': ObjectId(order.ids)}, { "$set": {'itemname':order.itemname, 
											'price': order.price, 
											'quantity': order.quantity, 
											'username': order.username}})

	def updateOrderQuantity(self, order):
		self.conn.update_one({'_id': ObjectId(order.ids)}, { "$inc": { 
											'quantity': order.quantity}})

	def deleteOrder(self, ids):
		self.conn.delete_one({'_id': ObjectId(ids)})