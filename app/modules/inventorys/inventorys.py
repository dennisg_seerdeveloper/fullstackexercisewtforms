from datetime import datetime
from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import request
from flask import url_for
from flask import g
from flask import flash
from ..forms import inventory_form
from ..models import user as UserRef
from ..models import inventory as InventoryRef
from functools import wraps

mod = Blueprint('inventorys', __name__)

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect('/login')
        else:
            return func(*args, **kwargs)
    return wrapper 

@mod.route('/')
def inventorys():
    form = inventory_form.AddInventoryForm(request.form)
    
    inventorys = g.inventorysdb.getInventoryAll()

    return render_template('inventorys/inventorys.html', form=form, inventorys=inventorys)

@mod.route('/', methods=['POST', 'GET'])
def create_inventory():
    
    form = inventory_form.AddInventoryForm(request.form)

    inventorys = g.inventorysdb.getInventoryAll()
    
    if request.method == 'POST' and form.validate():
        inventory = InventoryRef.Inventory(session['username'], form.itemname.data, form.price.data,
                    form.quantity.data)

        g.inventorysdb.createInventory(inventory)
        
        flash('New product inventory created!', 'create_inventory_success')
        return redirect(url_for('.inventorys'))

    return render_template('inventorys/inventorys.html', form=form, inventorys=inventorys)


@mod.route('/edit_inventory/<objid>', methods=['POST' ,'GET'])
def edit_inventory(objid):
    form = inventory_form.AddInventoryForm(request.form)

    inventory = g.inventorysdb.getInventorybyObjid(str(objid))

    if request.method == 'POST' and form.validate():
        
        ids = objid

        inventory = InventoryRef.Inventory(session['username'], form.itemname.data, form.price.data, form.quantity.data, ids)
        #return render_template('inventorys/inventorys_edit.html', inventoryh=inventory)
        g.inventorysdb.updateInventory(inventory)

        flash('Product inventory successfully updated!', 'updated_inventory_success')
        return redirect(url_for('.inventorys'))

    return render_template('inventorys/inventorys_edit.html', form=form, inventory=inventory)


@mod.route('/editquantity_inventory/<objid>', methods=['POST' ,'GET'])
def editquantity_inventory(objid):
    form = inventory_form.AddInventoryQuantityForm(request.form)

    inventory = g.inventorysdb.getInventorybyObjid(str(objid))

    if request.method == 'POST' and form.validate():
        
        ids = objid

        inventory = InventoryRef.Inventory(session['username'], '', '', form.quantity.data, ids)
        #return render_template('inventorys/inventorys_edit.html', inventoryh=inventory)
        g.inventorysdb.updateInventoryQuantity(inventory)

        flash('Product inventory quantity successfully updated!', 'updated_quantity_inventory_success')
        return redirect(url_for('.inventorys'))

    return render_template('inventorys/inventorys_quantityedit.html', form=form, inventory=inventory)

@mod.route('/shoppingcart/')
def shoppingcart():

    inventorys = g.inventorysdb.getInventoryAll()

    return render_template('inventorys/shoppingcart.html', inventorys=inventorys)


@mod.route('/addtocart/<objid>', methods=['POST' ,'GET'])
def addtocart(objid):

    addcarts = g.inventorysdb.getInventorybyObjid(str(objid))
        
    for addcart in addcarts:
            
        g.cartsdb.createCart(addcart['itemname'], addcart['price'], addcart['price'], addcart['quantity'], addcart['username'])

    viewcarts = g.cartsdb.getCartbyUsername(session['username'])

    return render_template('inventorys/inventorys_addcart.html', viewcarts=viewcarts)



