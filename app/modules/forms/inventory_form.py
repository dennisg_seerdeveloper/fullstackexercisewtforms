from wtforms import Form
from wtforms import BooleanField
from wtforms import TextField
from wtforms import PasswordField
from wtforms import FloatField
from wtforms import DecimalField
from wtforms import IntegerField
from wtforms import validators

class AddInventoryForm(Form):
	itemname = TextField('Item Name', [validators.Required(), validators.Regexp(r'^[\w.@+-]+$')])
	price = FloatField('Price')
	quantity = IntegerField('Quantity')


class AddInventoryQuantityForm(Form):
	quantity = IntegerField('Quantity')
		
    